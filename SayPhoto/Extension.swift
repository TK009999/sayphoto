//
//  Extension.swift
//  IOS_SearchJob
//
//  Created by UDN08791-D001 on 10/18/16.
//  Copyright © 2016 udn. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    func heightForComment(_ string: String, font: UIFont, width: CGFloat) -> CGFloat {
        let calculateString: NSString = NSString(string: string)
        let calculateSize: CGSize = CGSize(width: width, height: CGFloat(MAXFLOAT))
        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin]
        let mutableParagraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        mutableParagraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        let attributes:  [String : AnyObject] =  [NSFontAttributeName : font, NSParagraphStyleAttributeName: mutableParagraphStyle]
        let size = calculateString.boundingRect(with: calculateSize, options: options, attributes: attributes, context: nil).size
        return ceil(size.height)
    }
}

extension UIColor {
    func RGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func GetImageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func randomColor(_ openColor: Bool) -> UIColor {
        return openColor ? UIColor(
            
            red: CGFloat(CGFloat(arc4random()).truncatingRemainder(dividingBy: 256) / 256.0) + 1.0,
            green: CGFloat(CGFloat(arc4random()).truncatingRemainder(dividingBy: 256) / 256.0) + 0.5,
            blue: CGFloat(CGFloat(arc4random()).truncatingRemainder(dividingBy: 256) / 256.0) + 0.3,
            alpha: CGFloat(1.0)
            
        ) : UIColor.clear
    }
}

extension UIView {
    func enlargeSize(imageSize: CGSize, weightingCoefficient: CGFloat) -> CGSize {
        let scale = UIScreen.main.bounds.size.width / 320 + weightingCoefficient
        let calculatedWidth: CGFloat = imageSize.width * scale
        let calculatedHeight: CGFloat = imageSize.height  * scale
        let calculateSize: CGSize = CGSize(width: calculatedWidth, height: calculatedHeight)
        return calculateSize
    }
    
    func gainConstraintOfViewByAssign(view: UIView, beGainObject: AnyObject?, direction: NSLayoutAttribute) -> NSLayoutConstraint? {
        if view.constraints.count > 0 {
            for cons in view.constraints {
                if let object = beGainObject {
                    if cons.firstItem.tag == object.tag {
                        if cons.firstAttribute == direction {
                            return cons
                        }
                    }
                }
            }
        }
        return nil
    }
}

extension Dictionary {
//    func setValue(value: String) -> Int {
//        var key: Int = 0
//        for (_key, _value) in LOCATION_SEQUENCE_BY_KEY_NUMBER {
//            guard value != _value else {
//                key = _key
//                return key
//            }
//        }
//        return key
//    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = self.index(self.startIndex, offsetBy: r.lowerBound)
        let end = self.index(self.startIndex, offsetBy: (r.upperBound - r.lowerBound))
        return self[Range(start ..< end)]
    }
    
//    func stringWithRangeByLocation(string: String) -> String {
//        for number in 0..<LOCATION_SEQUENCE_BY_KEY_NUMBER.count {
//            if let range = string.range(of: LOCATION_SEQUENCE_BY_KEY_NUMBER[number]!) {
//                let lo = string.index(range.lowerBound, offsetBy: 0)
//                let hi = string.index(range.lowerBound, offsetBy: 2)
//                let subRange = lo ... hi
//                return string[subRange]
//            }
//        }
//        return ""
//    }
    
    func matcheResults(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch _ {
            //print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func isMatched(for regex: String, in text: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.count > 0
        } catch _ {
            //print("invalid regex: \(error.localizedDescription)")
            return false
        }
    }
}

extension CALayer {
    func shdowLayer(radius: CGFloat, shdowOpen: Bool) {
        guard shdowOpen == false else {
            self.shadowOffset = CGSize(width: 1.0, height: 1.0)
            self.shadowOpacity = 0.3
            self.shadowColor = UIColor.black.withAlphaComponent(1).cgColor
            self.shadowRadius = radius
            return
        }
    }
    
    func radiusLayer(radius: CGFloat, radiusOpen: Bool) {
        guard radiusOpen == false else {
            self.cornerRadius = radius
            self.masksToBounds = true
            return
        }
    }
    
    func borderLayer(borderWidth: CGFloat, borderColor: UIColor, borderOpen: Bool) {
        guard borderOpen == false else {
            self.borderWidth = borderWidth
            self.borderColor = borderColor.cgColor
            self.masksToBounds = true
            return
        }
    }
}

extension UIViewController {
    func initializeNavigationBarWithBackButtonItem(topItemTitle: String?, leftAction: Selector, otherButtomItem: UIBarButtonItem? = nil) {
        let left_spacer: UIBarButtonItem =  UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        left_spacer.width = -1
        
        let button = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width / 2, height: 50.0))
        button.addTarget(self, action: leftAction, for: UIControlEvents.touchUpInside)
        button.backgroundColor = .clear
        
        let buttonImageView: UIImageView = UIImageView(frame: CGRect(x: 0.0, y: button.frame.size.height / 2 - 25.0 / 2, width: 25.0, height: 25.0))
        buttonImageView.image = UIImage(named: "icon_back")
        buttonImageView.backgroundColor = .clear
        button.addSubview(buttonImageView)
        
        let topBarTitleLabel: UILabel = UILabel()
        topBarTitleLabel.font = UIFont.systemFont(ofSize: 22.0)
        topBarTitleLabel.text = topItemTitle
        topBarTitleLabel.textColor = UIColor.white
        topBarTitleLabel.backgroundColor = .clear
        topBarTitleLabel.sizeToFit()
        topBarTitleLabel.frame = CGRect(x: buttonImageView.frame.size.width, y: button.frame.size.height / 2 - topBarTitleLabel.frame.size.height / 2, width: topBarTitleLabel.frame.size.width, height: topBarTitleLabel.frame.size.height)
        button.addSubview(topBarTitleLabel)
        
        let buttonContainer: UIView = UIView(frame: CGRect(x: 0, y: 0, width: button.frame.width, height: button.frame.height))
        buttonContainer.addSubview(button)
        let exitButtonItem: UIBarButtonItem = UIBarButtonItem(customView: buttonContainer)
        self.navigationItem.leftBarButtonItems = [left_spacer, exitButtonItem]
        
//        let attrs: [String : AnyObject] = [
//            NSFontAttributeName : UIFont.systemFont(ofSize: 22.0),
//            NSForegroundColorAttributeName : UIColor.white
//        ]
//        self.navigationController?.navigationBar.titleTextAttributes = attrs
//        self.navigationController?.navigationBar.topItem?.title = topItemTitle
        let right_spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        right_spacer.width = -1
        self.navigationItem.rightBarButtonItems = otherButtomItem != nil ? [right_spacer, otherButtomItem!] : nil
    }
}

private var pTouchAreaEdgeInsets: UIEdgeInsets = .zero
extension UIButton {
    func setImageAndTitle() {
        let SPACING: CGFloat = 6.0
        self.setImageAndTitleLeft(spacing: SPACING)
    }
    
    //Image View at up, Label at Bottom
    func setImageAndTitleLeft(spacing: CGFloat) {
        let offset: CGFloat = spacing;
        self.imageEdgeInsets = UIEdgeInsetsMake(-self.titleLabel!.intrinsicContentSize.height - offset, 0.0, 0.0, -self.titleLabel!.intrinsicContentSize.width);
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -self.imageView!.frame.size.width, -self.imageView!.frame.size.height - offset, 0.0);
        // button.imageEdgeInsets = UIEdgeInsetsMake(-button.titleLabel.frame.size.height-offset/2, 0, 0, -button.titleLabel.frame.size.width);
        // 由于iOS8中titleLabel的size为0，用上面这样设置有问题，修改一下即可
    }
    
    //設定點擊範圍
    var touchAreaEdgeInsets: UIEdgeInsets {
        get {
            if let value = objc_getAssociatedObject(self, &pTouchAreaEdgeInsets) as? NSValue {
                var edgeInsets: UIEdgeInsets = .zero
                value.getValue(&edgeInsets)
                return edgeInsets
            }
            else {
                return .zero
            }
        }
        set(newValue) {
            var newValueCopy = newValue
            let objCType = NSValue(uiEdgeInsets: .zero).objCType
            let value = NSValue(&newValueCopy, withObjCType: objCType)
            objc_setAssociatedObject(self, &pTouchAreaEdgeInsets, value, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if UIEdgeInsetsEqualToEdgeInsets(self.touchAreaEdgeInsets, .zero) || !self.isEnabled || self.isHidden {
            return super.point(inside: point, with: event)
        }
        
        let relativeFrame = self.bounds
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, self.touchAreaEdgeInsets)
        
        return hitFrame.contains(point)
    }
}

extension UIImage {
    
    func tintWithColor(_ color: UIColor) -> UIImage {
        
        UIGraphicsBeginImageContext(self.size)
        let context = UIGraphicsGetCurrentContext()
        
        // flip the image
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.translateBy(x: 0.0, y: -self.size.height)
        
        // multiply blend mode
        context?.setBlendMode(CGBlendMode.multiply)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context?.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context?.fill(rect)
        
        // create uiimage
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        //println("uname(&systemInfo): \(uname(&systemInfo))")
        let machine = systemInfo.machine
        //println("machine: \(machine)")
        let mirror = Mirror(reflecting: machine)
        //println("mirror: \(mirror)")
        
        let identifier = mirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
