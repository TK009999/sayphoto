//
//  CustomMPVolume.swift
//  TalkPhoto
//
//  Created by HaochengLee on 11/07/2017.
//  Copyright © 2017 HaoCheng Lee. All rights reserved.
//

import UIKit
import MediaPlayer

class CustomMPVolume: MPVolumeView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var defaultValue: CGFloat = 0.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
