//
//  File.swift
//  SearchJob
//
//  Created by UDN08791-D001 on 2015/6/1.
//  Copyright (c) 2015年 udn. All rights reserved.
//

import UIKit

class Constraint {
    
    class func Custom(_ item: AnyObject, itemAttribute:NSLayoutAttribute , toItem: AnyObject? ,toItemAttribute:NSLayoutAttribute, constant: CGFloat) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: itemAttribute , relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: toItemAttribute, multiplier: 1.0, constant: constant)
        return Constraint
    }
    
    class func TargetTop(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func TargetBottom(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func TargetLeft(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func TargetRight(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func Top(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func Bottom(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func Trailing(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func Leading(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func Width(_ item: AnyObject, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func Height(_ item: AnyObject, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func CenterX(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func CenterY(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat? = 0.0) -> NSLayoutConstraint {
        let Constraint =  NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: constant!)
        return Constraint
    }
    
    class func CustomCenterX(_ item: AnyObject, toItem: AnyObject?, setconstant: CGFloat) -> NSLayoutConstraint {
        let Constraint =  NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: setconstant)
        return Constraint
    }
    
    class func CustomCenterY(_ item: AnyObject, toItem: AnyObject?, setconstant: CGFloat) -> NSLayoutConstraint {
        let Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: setconstant)
        return Constraint
    }
    
}
