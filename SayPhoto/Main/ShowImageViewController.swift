//
//  ShowImageViewController.swift
//  TalkPhoto
//
//  Created by HaochengLee on 08/07/2017.
//  Copyright © 2017 HaoCheng Lee. All rights reserved.
//

import UIKit

class ShowImageViewController: UIViewController {
    
    var image: UIImage! = nil
    
    let showImageView: UIImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor().RGB(0x272727)
        self.navigationController?.setToolbarHidden(false, animated: false)
        
        var items: [UIBarButtonItem] = []
        
        items.append(
            UIBarButtonItem(barButtonSystemItem: .action, target: self, action: nil)
        )
        
        items.append(
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        )
    
        self.navigationController?.toolbar.items = items
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showImageView.frame = self.view.frame
        self.showImageView.contentMode = .scaleAspectFit
        self.showImageView.image = self.image
        self.view.addSubview(self.showImageView)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
