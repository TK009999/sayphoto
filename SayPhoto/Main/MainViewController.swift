//
//  MainViewController.swift
//  TalkPhoto
//
//  Created by HaochengLee on 06/07/2017.
//  Copyright © 2017 HaoCheng Lee. All rights reserved.
//

import UIKit
import AVFoundation
import Speech
import MobileCoreServices
import QuartzCore
import MediaPlayer

class MainViewController: UIViewController {
    
    // All Of Buttons On Main View
    var voiceRecognizeBtn: UIButton! = nil
    var voiceRecognizeBtnSpinEffect: UIImageView! = nil
    var transformCameraBtn: UIButton! = nil
    var settingBtn: UIButton! = nil
    var albumBtn: UIButton! = nil
    
    // These For Custom Camera
    var session: AVCaptureSession! = nil
    var stillImageOutput: AVCapturePhotoOutput! = nil
    var videoPreviewLayer: AVCaptureVideoPreviewLayer! = nil
    var captureDevice: AVCaptureDevice!
    var keywordListTableView: UITableView! = nil
    
    // These For Speech Recognition
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest!
    var audioEngine: AVAudioEngine!
    var speechRecognizer: SFSpeechRecognizer!
    var recognitionTask: SFSpeechRecognitionTask!
    var isRecording = false
    
    // Definition of Attribute
    let kKeywordListTableViewCell: String = "kKeywordListTableViewCell"
    
    // User Default Data Save Key
    let kDefaultVolume: String = "kDefaultVolume"
    
    // Keyword of Array
    let keywordList: [String] = ["photo", "chesse", "pi", "show", "look"]
    
    // Animated Status for Keyword List
    var keywordListAnimatedStatus: listAnimatedStatus = .down
    enum listAnimatedStatus: Int {
        case up = 0
        case down = 1
    }
    
    // CALayer
    var circleShape: CALayer! = nil
    
    // When Capture animate a temporary image view into album
    var temporaryImageView: UIImageView! = nil
    
    // For Photo Library
    let imagePicker: UIImagePickerController = UIImagePickerController()
    
    // Beacuse of Hidden Sysytem Volume View
    let volumeView = CustomMPVolume(frame: .zero)
    
    override func loadView() {
        super.loadView()
        guard self.view == nil else { return self.view.backgroundColor = .clear }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.bounds = UIScreen.main.bounds
        self.buildMPVolume()
        self.configureCustomCamera(view: self.view)
        self.configureVoiceRecognizeBtn(view: self.view)
        // MARK: Next Version
//        self.configureTransformCameraBtn(view: self.view)
//        self.configureSettingBtn(view: self.view)
        self.configureAlbumBtn(view: self.view)
        self.configureKeywordListTableView(view: self.view)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startVoiceRecognize()
        self.listenVolumeButtonStart()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopVoiceRecognize()
        self.listenVolumeButtonStop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configure keywordListTableView
    func configureKeywordListTableView(view: UIView) {
        self.keywordListTableView = UITableView()
        self.keywordListTableView.delegate = self
        self.keywordListTableView.dataSource = self
        self.keywordListTableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: self.kKeywordListTableViewCell)
        self.keywordListTableView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.keywordListTableView.separatorStyle = .none
        self.keywordListTableView.layer.radiusLayer(radius: 25.0, radiusOpen: true)
//        self.keywordListTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.keywordListTableView)
//        let constraints: [NSLayoutConstraint] = [
//            Constraint.Top(self.keywordListTableView, toItem: view),
//            Constraint.Leading(self.keywordListTableView, toItem: view),
//            Constraint.Bottom(self.keywordListTableView, toItem: view),
//            Constraint.Trailing(self.keywordListTableView, toItem: view)
//        ]
//        view.addConstraints(constraints)
    }
    
    // MARK: - Configure voiceRecognizeBtn
    func configureVoiceRecognizeBtn(view: UIView) {
        
        // voiceRecognizeBtn
        self.voiceRecognizeBtn = UIButton()
        self.voiceRecognizeBtn.setBackgroundImage(#imageLiteral(resourceName: "Button_7"), for: .selected)
        self.voiceRecognizeBtn.setBackgroundImage(#imageLiteral(resourceName: "Button_7_stop"), for: .normal)
        self.voiceRecognizeBtn.addTarget(self, action: #selector(self.voiceRecognizeBtnEvent(_:)), for: .touchUpInside)
        self.voiceRecognizeBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.voiceRecognizeBtn)
        let constraints: [NSLayoutConstraint] = [
            Constraint.Width(self.voiceRecognizeBtn, constant: 80.0),
            Constraint.Height(self.voiceRecognizeBtn, constant: 80.0),
            Constraint.Bottom(self.voiceRecognizeBtn, toItem: view),
            Constraint.CenterX(self.voiceRecognizeBtn, toItem: view)
        ]
        view.addConstraints(constraints)
        
        // voiceRecognizeBtnSpinEffect
        self.voiceRecognizeBtnSpinEffect = UIImageView()
        self.voiceRecognizeBtnSpinEffect.image = #imageLiteral(resourceName: "circle")
        self.voiceRecognizeBtnSpinEffect.translatesAutoresizingMaskIntoConstraints = false
        self.voiceRecognizeBtn.addSubview(self.voiceRecognizeBtnSpinEffect)
        let constraints_spin: [NSLayoutConstraint] = [
            Constraint.Width(self.voiceRecognizeBtnSpinEffect, constant: 80.0),
            Constraint.Height(self.voiceRecognizeBtnSpinEffect, constant: 80.0),
            Constraint.CenterX(self.voiceRecognizeBtnSpinEffect, toItem: self.voiceRecognizeBtn),
            Constraint.CenterY(self.voiceRecognizeBtnSpinEffect, toItem: self.voiceRecognizeBtn)
        ]
        self.voiceRecognizeBtn.addConstraints(constraints_spin)
    }
    
    func voiceRecognizeBtnEvent(_ button: UIButton) {
        print("Tapped voiceRecognizeBtnEvent.")
        if !button.isSelected {
            self.startVoiceRecognize()
        } else {
            self.stopVoiceRecognize()
        }
    }
    
    // MARK: - Configure transformCamaraBtn
    func configureTransformCameraBtn(view: UIView) {
        self.transformCameraBtn = UIButton()
        self.transformCameraBtn.setBackgroundImage(#imageLiteral(resourceName: "transform"), for: .highlighted)
        self.transformCameraBtn.setBackgroundImage(#imageLiteral(resourceName: "transform"), for: .normal)
        self.transformCameraBtn.addTarget(self, action: #selector(self.transformCameraBtnEvent(_:)), for: .touchUpInside)
        self.transformCameraBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.transformCameraBtn)
        let size: CGSize = #imageLiteral(resourceName: "transform").size
        let constraints: [NSLayoutConstraint] = [
            Constraint.Width(self.transformCameraBtn, constant: size.width),
            Constraint.Height(self.transformCameraBtn, constant: size.height),
            Constraint.CenterY(self.transformCameraBtn, toItem: self.voiceRecognizeBtn),
            Constraint.Trailing(self.transformCameraBtn, toItem: view, constant: -20.0)
        ]
        view.addConstraints(constraints)
    }
    
    func transformCameraBtnEvent(_ button: UIButton) {
        print("Tapped transformCamaraBtnEvent.")
    }
    
    // MARK: - Configure settingBtn
    func configureSettingBtn(view: UIView) {
        self.settingBtn = UIButton()
        self.settingBtn.setBackgroundImage(#imageLiteral(resourceName: "set"), for: .highlighted)
        self.settingBtn.setBackgroundImage(#imageLiteral(resourceName: "set"), for: .normal)
        self.settingBtn.addTarget(self, action: #selector(self.settingBtnEvent(_:)), for: .touchUpInside)
        self.settingBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.settingBtn)
        let size: CGSize = #imageLiteral(resourceName: "transform").size
        let constraints: [NSLayoutConstraint] = [
            Constraint.Width(self.settingBtn, constant: size.width),
            Constraint.Height(self.settingBtn, constant: size.height),
            Constraint.Top(self.settingBtn, toItem: view, constant: 20.0),
            Constraint.Leading(self.settingBtn, toItem: view, constant: 20.0)
        ]
        view.addConstraints(constraints)
    }
    
    func settingBtnEvent(_ button: UIButton) {
        print("Tapped settingBtnEvent.")
    }
    
    // MARK: - Configure albumBtn
    func configureAlbumBtn(view: UIView) {
        self.albumBtn = UIButton()
        self.albumBtn.setBackgroundImage(#imageLiteral(resourceName: "album"), for: .highlighted)
        self.albumBtn.setBackgroundImage(#imageLiteral(resourceName: "album"), for: .normal)
        self.albumBtn.addTarget(self, action: #selector(self.albumBtnEvent(_:)), for: .touchUpInside)
        self.albumBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.albumBtn)
        let size: CGSize = #imageLiteral(resourceName: "transform").size
        let constraints: [NSLayoutConstraint] = [
            Constraint.Width(self.albumBtn, constant: size.width),
            Constraint.Height(self.albumBtn, constant: size.height),
            Constraint.Top(self.albumBtn, toItem: view, constant: 20.0),
            Constraint.Trailing(self.albumBtn, toItem: view, constant: -20.0)
        ]
        view.addConstraints(constraints)
    }
    
    func albumBtnEvent(_ button: UIButton) {
        print("Tapped albumBtnEvent.")
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = [String(kUTTypeImage)]
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }

}

// MARK: UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // Configure customCamera
    func configureCustomCamera(view: UIView) {
        
        self.session = AVCaptureSession()
        self.session.sessionPreset = AVCaptureSessionPresetPhoto
        
        //NOTE: If you plan to upload your photo to Parse, you will likely need to change your preset to AVCaptureSessionPresetHigh or AVCaptureSessionPresetMedium to keep the size under the 10mb Parse max.
        
        self.captureDevice = self.cameraWithPosition(position: .front)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: self.captureDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        if error == nil && self.session.canAddInput(input) {
            self.session.addInput(input)
            // ...
            // The remainder of the session setup will go here...
        }
        
        self.stillImageOutput = AVCapturePhotoOutput()
        let settings: AVCapturePhotoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
        self.stillImageOutput.photoSettingsForSceneMonitoring = settings
        
        if self.session.canAddOutput(self.stillImageOutput) {
            self.session.addOutput(self.stillImageOutput)
            // ...
            // Configure the Live Preview here...
            self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.session)
            self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.videoPreviewLayer.connection?.videoOrientation = .portrait
            self.videoPreviewLayer.frame = view.bounds
            
            view.layer.addSublayer(self.videoPreviewLayer)
            
            let focusGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapToFocusEvent(_:)))
            focusGesture.cancelsTouchesInView = false
            focusGesture.numberOfTapsRequired = 1
            focusGesture.numberOfTouchesRequired = 1
            view.addGestureRecognizer(focusGesture)
            self.session.startRunning()
        }
    }
    
    // Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
    func cameraWithPosition(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        if let discoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaTypeVideo, position: .unspecified) {
            for device in discoverySession.devices {
                if device.position == position {
                    return device
                }
            }
        }
        
        return nil
    }
    
    // tap To Focus Event
    func tapToFocusEvent(_ gesture: UITapGestureRecognizer) {
        
        guard let previewLayer = self.videoPreviewLayer else {
            print("Expected a previewLayer")
            return
        }
        
        guard let device = self.captureDevice else {
            print("Expected a device")
            return
        }
        
        let touchPoint: CGPoint = gesture.location(in: self.view)
        let convertedPoint: CGPoint = previewLayer.captureDevicePointOfInterest(for: touchPoint)
        if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(AVCaptureFocusMode.autoFocus) {
            do {
                try device.lockForConfiguration()
                device.focusPointOfInterest = convertedPoint
                device.focusMode = .autoFocus
                device.unlockForConfiguration()
            } catch {
                print("unable to focus")
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let showImageViewController: ShowImageViewController = ShowImageViewController()
            showImageViewController.image = image
            
            picker.pushViewController(showImageViewController, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Recording, Recognize Speech, Authorization Status
extension MainViewController {
    
    // Start Voice Recognize
    func startVoiceRecognize() {
        
        if self.voiceRecognizeBtn.isSelected == false {
            DispatchQueue.main.async(execute: {
                self.voiceRecognizeBtn.isSelected = true
                self.spinAnimationOn(view: self.voiceRecognizeBtnSpinEffect, duration: 3, rotations: 0.3, isRepeat: true)
                self.rippleEffectOn(view: self.voiceRecognizeBtn)
                self.recordAndRecognizeSpeech()
                self.isRecording = true
                
                self.animateOn(table: self.keywordListTableView, status: .up)
            })
        }
    }
    
    // Stop Voice Recognize
    func stopVoiceRecognize() {
        if self.voiceRecognizeBtn.isSelected {
            DispatchQueue.main.async(execute: {
                self.voiceRecognizeBtn.isSelected = false
                self.stopAllAnimations(view: self.voiceRecognizeBtnSpinEffect)
                self.stopAllAnimations(view: self.voiceRecognizeBtn)
                self.stopRecognizeSpeech()
                self.isRecording = false
                
                self.animateOn(table: self.keywordListTableView, status: .down)
            })
        }
    }
    
    func stopRecognizeSpeech() {
        
        self.audioEngine.stop()
        
        if let inputNode = self.audioEngine.inputNode {
           inputNode.removeTap(onBus: 0)
        }
        
        if self.recognitionTask != nil {
            self.recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        self.recognitionRequest.endAudio()
        self.recognitionRequest = nil
        
        self.speechRecognizer.queue.cancelAllOperations()
        self.speechRecognizer = nil
    }
    
    func requestSpeechAuthorization() {
        SFSpeechRecognizer.requestAuthorization { (SFSpeechRecognizerAuthorizationStatus) in
            switch SFSpeechRecognizerAuthorizationStatus {
            case .authorized:
                self.voiceRecognizeBtn.isEnabled = true
            case .denied:
                self.voiceRecognizeBtn.isEnabled = false
            case .restricted:
                self.voiceRecognizeBtn.isEnabled = false
            case .notDetermined:
                self.voiceRecognizeBtn.isEnabled = false
            }
        }
    }
    
    func recordAndRecognizeSpeech() {
        
        self.audioEngine = AVAudioEngine()
        self.speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-AU"))
        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = self.audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = self.recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest.append(buffer)
        }
        
        self.audioEngine.prepare()
        
        do {
            try self.audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        // Assign to a New Task of Speech recognition
        DispatchQueue.global(qos: .default).async(execute: {
            self.recognitionTask = self.speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: self.speechRecognitionTask)
        })
        
    }
    
    // Speech Recognition Task
    func speechRecognitionTask(result: SFSpeechRecognitionResult?, error: Error?){
        
        var isFinal = false
        
        if let result = result {
            
            isFinal = result.isFinal
            
            let bestString = result.bestTranscription.formattedString
            
            var lastString: String = ""
            for segment in result.bestTranscription.segments {
                let indexTo = bestString.index(bestString.startIndex, offsetBy: segment.substringRange.location)
                lastString = bestString.substring(from: indexTo)
            }
            
            self.takenPhotoByTalk(resultString: lastString.lowercased())
        }
        
        if isFinal {
            self.stopVoiceRecognize()
            self.sendAlert(title: "Recognizer time out restart it, Please")
        } else if let error = error {
            self.stopVoiceRecognize()
            print("error2 = \(error)")
        }
        
    }
    
    // Taken Photo by Talk
    func takenPhotoByTalk(resultString: String) {
        
        print("The resultString is \"\(resultString)\"")
        
        // Check the result String whether inclusive in keyword List or not
        if self.keywordList.contains(resultString) {
            print("match the one of the keywords")
            
            self.stillImageOutput.connection(withMediaType: AVMediaTypeVideo)
            
            let settings: AVCapturePhotoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
            
            self.stillImageOutput.capturePhoto(with: settings, delegate: self)
            
        } else {
            self.sendAlert(title: "Say keyword of list on screen to take photo, Please")
        }
    }
    
    // Build MPVolume View beacuse of to Hidden System Volume View
    func buildMPVolume() {
        self.view.addSubview(self.volumeView)
    }
    
    // Listen Volume Button Taped
    func listenVolumeButtonStart() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
//            try audioSession.setCategory(AVAudioSessionCategoryRecord)
//            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        audioSession.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.new, context: nil)
        
//        UserDefaults.standard.set(audioSession.outputVolume, forKey: self.kDefaultVolume)
//        if UserDefaults.standard.synchronize() {
//            print("data saved successfully to disk")
//        } else {
//            print("data saved error")
//        }
    }
    
    func listenVolumeButtonStop() {
        let audioSession = AVAudioSession.sharedInstance()
        audioSession.removeObserver(self, forKeyPath: "outputVolume")
    }
    
    // Notification the Volume Contorl
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "outputVolume" {
//            print("got in here")
//        }

        
        if keyPath == "outputVolume" {
            
            if let change = change {
                
                if let value = change[NSKeyValueChangeKey.newKey] as? CGFloat {
                    
                    if let currentValue = UserDefaults.standard.value(forKey: self.kDefaultVolume) as? CGFloat {
                        self.volumeView.defaultValue = currentValue
                    }
                    
                    if value > self.volumeView.defaultValue {
                        self.startVoiceRecognize()
                        
                        print("Touch zoom in the Volume \(value)")
                        self.volumeView.defaultValue = value
                        
                    } else if value < self.volumeView.defaultValue {
                        self.stopVoiceRecognize()
                        
                        print("Touch zoom out the Volume \(value)")
                        self.volumeView.defaultValue = value
                    }
                    
                    UserDefaults.standard.set(self.volumeView.defaultValue, forKey: self.kDefaultVolume)
                    if UserDefaults.standard.synchronize() {
                        print("data saved successfully to disk")
                    } else {
                        print("data saved error")
                    }
                }
            }

    
//            print("object \(object)")
//            print("change \(change?[NSKeyValueChangeKey.newKey])")
//            print("context \(context)")
//            print("got in here")
        }
    }
}

// MARK: - AVCapturePhotoCaptureDelegate
extension MainViewController: AVCapturePhotoCaptureDelegate {
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if photoSampleBuffer != nil {
            let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: nil)
            let dataProvider = CGDataProvider.init(data: imageData! as CFData)
            let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.perceptual)
            
            var orientation: UIImageOrientation = .downMirrored
            switch self.captureDevice.position {
            case .front:
                orientation = .leftMirrored
            case .back:
                orientation = .leftMirrored
            case .unspecified:
                orientation = .upMirrored
            }
            
            let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: orientation)
            
            self.runAnimationOnBezierPath(image: image)
            
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
            //            self.captrueImageView.image = image
            
            // ...
            // Add the image to captureImageView here...
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo: UnsafeRawPointer) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                                          message: "Failed to save image",
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                                             style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true,
                         completion: nil)
        }
    }
}

// MARK: - About The Bezier Path & CAAnimationDelegate
extension MainViewController: CAAnimationDelegate {
    
//    func testAnimate(withDuration duration: TimeInterval, animations: @escaping () -> Void, completion: ((Bool) -> Void)? = nil) {
//        UIView.animate(withDuration: duration, animations: animations, completion: completion)
//    }
    
    func runAnimationOnBezierPath(image: UIImage) {
        
        //        self.redImageView.transform = .identity
        //        self.redImageView.alpha = 1.0
        
        //三阶贝塞尔曲线
        let mainPath4: UIBezierPath = UIBezierPath()
        mainPath4.move(to: self.view.center)
        mainPath4.addQuadCurve(to: self.albumBtn.center, controlPoint: CGPoint(x: 150.0, y: 50.0))
        
        //        let shapeLayer = CAShapeLayer()
        //        shapeLayer.path = mainPath4.cgPath //存入UIBezierPath的路径
        //        shapeLayer.fillColor = UIColor.clear.cgColor //设置填充色
        //        shapeLayer.lineWidth = 5  //设置路径线的宽度
        //        shapeLayer.strokeColor = UIColor.gray.cgColor //路径颜色
        //        //如果想变为虚线设置这个属性，[实线宽度，虚线宽度]，若两宽度相等可以简写为[宽度]
        //        shapeLayer.lineDashPattern = [2]
        //        //一般可以填"path"  "strokeStart" "strokeEnd"  具体还需研究
        //        let baseAnimation = CABasicAnimation(keyPath: "strokeEnd")
        //        baseAnimation.duration = 0   //持续时间
        //        baseAnimation.fromValue = 0  //开始值
        //        baseAnimation.toValue = 1    //结束值
        //        baseAnimation.repeatDuration = 1  //重复次数
        //        shapeLayer.add(baseAnimation, forKey: nil) //给ShapeLayer添
        //        //显示在界面上
        //        self.view.layer.addSublayer(shapeLayer)
        
        
        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        pathAnimation.path = mainPath4.cgPath
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnimation.toValue = NSValue(caTransform3D: CATransform3DMakeScale(0.0, 0.0, 1.0))
        
//        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
//        alphaAnimation.fromValue = 1.0
//        alphaAnimation.toValue = 0.0
        
        let animation: CAAnimationGroup = CAAnimationGroup()
        animation.delegate = self
        animation.duration = 0.8
//        animation.repeatDuration = 0.8
        animation.animations = [pathAnimation, scaleAnimation]

        
        //        UIGraphicsBeginImageContext(self.view.frame.size)
        //        self.view.layer.render(in: UIGraphicsGetCurrentContext()!)
        //        let _image = UIGraphicsGetImageFromCurrentImageContext()
        //        UIGraphicsEndImageContext()
        //        let imageTest = UIImage(cgImage: _image!.cgImage!)
        
        if self.temporaryImageView != nil {
            self.temporaryImageView.removeFromSuperview()
            self.temporaryImageView = nil
        }
        
        self.temporaryImageView = UIImageView()
        self.temporaryImageView.frame = self.view.frame
        self.temporaryImageView.backgroundColor = .clear
        self.temporaryImageView.image = image
        self.temporaryImageView.layer.add(animation, forKey: nil)
        self.view.addSubview(self.temporaryImageView)
        
//        UIView.animate(withDuration: 1.0, animations: {
//            self.temporaryImageView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        }) {
//            (finish) in
//            self.temporaryImageView.layer.add(animation, forKey: nil)
//        }
    }
    
    func animationDidStart(_ anim: CAAnimation) {
        
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.temporaryImageView.isHidden = true
        self.temporaryImageView.layer.removeAllAnimations()
        self.temporaryImageView.removeFromSuperview()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Please say keyword of below."
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        if let header = view as? UITableViewHeaderFooterView {
            header.tintColor = .clear
            header.textLabel?.textColor = UIColor.white
            header.textLabel?.textAlignment = .center
            header.textLabel?.numberOfLines = 2
            
            var fromFrame: CGRect! = nil
            var toFrame: CGRect! = nil
            
            switch self.voiceRecognizeBtn.isSelected {
            case true:
                fromFrame = CGRect(x: UIScreen.main.bounds.width, y: header.frame.origin.y, width: header.frame.size.width, height: header.frame.size.height)
                toFrame = CGRect(x: 0.0, y: header.frame.origin.y, width: header.frame.size.width, height: header.frame.size.height)
            case false:
                fromFrame = CGRect(x: 0.0, y: header.frame.origin.y, width: header.frame.size.width, height: header.frame.size.height)
                toFrame = CGRect(x: UIScreen.main.bounds.width, y: header.frame.origin.y, width: header.frame.size.width, height: header.frame.size.height)
            }
            header.frame = fromFrame
            UIView.animate(withDuration: 1.0, animations: {
                header.frame = toFrame
            }, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: self.kKeywordListTableViewCell)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: self.kKeywordListTableViewCell)
        }
        
        cell.textLabel?.text = self.keywordList[indexPath.row]
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.textColor = .white
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        return cell
    }
    
    func animateOn(table: UITableView, status: listAnimatedStatus) {
        
        let originallySize: CGSize = UIScreen.main.bounds.size
        let y: CGFloat = originallySize.height
        let _width: CGFloat = 275.0
        
        var fromTransform: CGAffineTransform! = nil
        var toTransform: CGAffineTransform! = nil
        
        switch self.voiceRecognizeBtn.isSelected {
        case true:
            table.frame.origin = CGPoint(x: (originallySize.width - _width) / 2, y: y)
            table.frame.size = CGSize(width: _width, height: _width)
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .transitionCrossDissolve, animations: {
                table.frame.origin = CGPoint(x: (originallySize.width - _width) / 2, y: (originallySize.height - _width) / 2)
            }, completion: nil)
            
            fromTransform = CGAffineTransform(translationX: 0.0, y: 568.0)
            toTransform = CGAffineTransform(translationX: 0.0, y: 0.0)
            
            let cells: [UITableViewCell] = table.visibleCells
            
            for i in cells {
                let cell: UITableViewCell = i as UITableViewCell
                cell.transform = fromTransform
            }
            
            var index: Int = 0
            
            for a in cells {
                let cell: UITableViewCell = a as UITableViewCell
                UIView.animate(withDuration: 1.5, delay: 0.2 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                    cell.transform = toTransform
                }, completion: nil)
                index += 1
            }
            
            break
            
        case false:
            
            fromTransform = CGAffineTransform(translationX: 0.0, y: 0.0)
            toTransform = CGAffineTransform(translationX: 0.0, y: 568.0)
            
            let cells: [UITableViewCell] = table.visibleCells
            
            for i in cells {
                let cell: UITableViewCell = i as UITableViewCell
                cell.transform = fromTransform
            }
            
            var index: Int = 0
            
            for a in cells.reversed() {
                let cell: UITableViewCell = a as UITableViewCell
                UIView.animate(withDuration: 1.5, delay: 0.2 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                    cell.transform = toTransform
                }, completion: nil)
                index += 1
            }
            
            table.frame.origin = CGPoint(x: (originallySize.width - _width) / 2, y: (originallySize.height - _width) / 2)
            table.frame.size = CGSize(width: _width, height: _width)
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .transitionCrossDissolve, animations: {
                table.frame.origin = CGPoint(x: (originallySize.width - _width) / 2, y: y)
            }, completion: nil)
            
            break
        }
    }
}

// MARK: - Animation Spin, Ripple Effect
extension MainViewController {
    
    func spinAnimationOn(view: UIView, duration: CGFloat, rotations: CGFloat, isRepeat: Bool) {
        let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = CGFloat(.pi * 2.0) * rotations * duration
        rotationAnimation.duration = CFTimeInterval(duration)
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = isRepeat == true ? .infinity : 0
        view.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func rippleEffectOn(view: UIView) {
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnimation.toValue = NSValue(caTransform3D: CATransform3DMakeScale(3.0, 3.0, 1.0))
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 1.0
        alphaAnimation.toValue = 0.0
        let animation = CAAnimationGroup()
        animation.animations = [scaleAnimation, alphaAnimation]
        animation.duration = 1.5
        animation.repeatCount = .infinity
        
        let width = view.bounds.size.width
        let height = view.bounds.size.height
        let x = -view.bounds.midX
        let y = -view.bounds.midY
        
        self.circleShape = self.createCircleShapeWithPosition(position:            CGPoint(x: width / 2, y: height / 2), pathRect: CGRect(x: x, y: y, width: width, height: height), radius: width / 2)
        
//        let circleShape = self.createImageShapeWithPosition(position: CGPoint(x: width / 2, y: height / 2), pathRect: CGRect(x: x, y: y, width: self.scallView.image!.size.width, height: self.scallView.image!.size.height))

//        circleShape.contents = (self.scallView.image!.cgImage! as AnyObject)
        
        self.circleShape.add(animation, forKey: nil)
        
        view.layer.addSublayer(self.circleShape)
    }
    
    func createImageShapeWithPosition(position: CGPoint, pathRect rect: CGRect) -> CALayer {
        let imageLayer: CALayer = CALayer()
        imageLayer.bounds = rect
        imageLayer.position = position
        return imageLayer
    }
    
    func createCircleShapeWithPosition(position: CGPoint, pathRect rect: CGRect, radius: CGFloat) -> CAShapeLayer {
        let circleShape = CAShapeLayer()
        circleShape.path = self.createCirclePathWithRadius(frame: rect, radius: radius)
        circleShape.position = position
        circleShape.fillColor = UIColor.clear.cgColor
        circleShape.strokeColor = UIColor.white.cgColor
        circleShape.opacity = 1.0
        circleShape.lineWidth = 0.5
        return circleShape
    }
    
    func createCirclePathWithRadius(frame: CGRect, radius: CGFloat) -> CGPath {
        return UIBezierPath(roundedRect: frame, cornerRadius: radius).cgPath
    }
    
    func stopAllAnimations(view: UIView) {
        view.layer.removeAllAnimations()
        if self.circleShape != nil {
            self.circleShape.removeAllAnimations()
            self.circleShape.removeFromSuperlayer()
        }
    }
}

//MARK: - Alert
extension MainViewController {
    func sendAlert(title: String = "", message: String = "") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
