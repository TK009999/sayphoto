//
//  SettingTableViewController.swift
//  TalkPhoto
//
//  Created by HaochengLee on 28/06/2017.
//  Copyright © 2017 HaoCheng Lee. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {
    
    @IBAction func didTapConfirm(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let titleImageView: UIImageView = UIImageView()
        titleImageView.frame.size = #imageLiteral(resourceName: "set_up").size
        titleImageView.image = #imageLiteral(resourceName: "set_up")
        self.navigationItem.titleView = titleImageView
        
        self.tableView.register(SettingTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 3
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
        case 0:
            return 50.0
        case 1:
            return 50.0
        case 2:
            return 25.0
        case 3:
            return 50.0
        default:
            return 0.0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            
        case 0:
            return "Choose Model"
        case 1:
            return nil
        case 2:
            return nil
        case 3:
            return "Other"
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        switch indexPath.section {
            
        case 0:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Manually"
            default:
                cell.textLabel?.text = "Automatically"
            }
        case 1:
            cell.textLabel?.text = "Languages"
        case 2:
            cell.textLabel?.text = "---------"
        case 3:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Introduction"
            case 1:
                cell.textLabel?.text = "Share"
            default:
                cell.textLabel?.text = "Contect us"
            }
        default:
            cell.textLabel?.text = ""
        }
//         Configure the cell...
        
        cell.textLabel?.textColor = .white
        cell.selectionStyle = .none
        cell.backgroundColor = self.navigationController?.navigationBar.barTintColor
        
        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    deinit {
        print("already deinit about the \(self.classForCoder)")
    }

}
